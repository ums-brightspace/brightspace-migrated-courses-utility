const Axios = require("axios");
const https = require("https");

exports.databaseCredentials = function() {
	let connString =
		"(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
		process.env.DB_HOST +
		")(PORT=" +
		process.env.DB_PORT +
		"))(CONNECT_DATA=(SERVER=DEDICATED)(SID=" +
		process.env.DB_SID +
		")))";
	let creds = {
		user: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		connectString: connString
	};

	return creds;
};

exports.axios = Axios.create({
	baseURL: process.env.LMS_ETL_HOST,
	timeout: Number(process.env.HTTP_REQUEST_TIMEOUT),
	headers: {
		Authorization: "Bearer " + process.env.JWT_TOKEN
	},
	httpsAgent: new https.Agent({ rejectUnauthorized: false })
});

exports.handleAxiosError = function(error) {
	let errObject = { Error: error.message };
	if (error.response) {
		errObject = {
			status: error.response.status,
			text: error.response.statusText,
			message: error.message
		};
	} else if (error.request) {
		errObject = { Error: "No response received", message: error.message };
	}

	return errObject;
};
