const winston = require("winston");

let console = new winston.transports.Console({
	level: "info",
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.simple()
	)
});

if (process.env.NODE_ENV !== "production") {
	console.level = "silly";
	console.format = winston.format.combine(
		winston.format.timestamp(),
		winston.format.colorize(),
		winston.format.simple()
	);
}

let file = new winston.transports.File({
	filename: "logs/log.log",
	level: "info",
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.json()
	)
});

const logger = winston.createLogger({
	exitOnError: false,
	transports: [
		console,
		new winston.transports.File({ filename: "logs/error.log", level: "error" }),
		file
	],
	exceptionHandlers: [console]
});

exports.log = function(level, message, data) {
	logger.log(level, message, data);
};

exports.error = function(message, data) {
	this.log("error", message, data);
};

exports.warn = function(message, data) {
	this.log("warn", message, data);
};

exports.info = function(message, data) {
	this.log("info", message, data);
};

exports.http = function(message, data) {
	this.log("http", message, data);
};

exports.verbose = function(message, data) {
	this.log("verbose", message, data);
};

exports.debug = function(message, data) {
	this.log("debug", message, data);
};

exports.silly = function(message, data) {
	this.log("silly", message, data);
};
