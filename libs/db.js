const helper = require("./helper");
const oracle = require("oracledb");
const logger = require("./logger");

exports.initDb = function() {
	return new Promise(function(resolve, reject) {
		const creds = helper.databaseCredentials();
		oracle
			.createPool({
				user: creds.user,
				password: creds.password,
				connectString: creds.connectString,
				poolMax: 100,
				poolMin: 2
			})
			.then(function(pool) {
				resolve(pool.status);
			})
			.catch(function(err) {
				reject(err.message);
			});
	});
};

exports.getConnection = function() {
	return new Promise(function(resolve, reject) {
		oracle
			.getConnection()
			.then(function initiateDbConn(conn) {
				conn.execute(
					"alter session set nls_timestamp_format = 'YYYY-MM-DD HH24:MI:SS'"
				);
				resolve(conn);
			})
			.catch(function() {
				reject("Error getting connection from pool");
			});
	});
};

exports.closeDb = function() {
	return new Promise(function(resolve, reject) {
		oracle
			.getPool()
			.close(3)
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err.message);
			});
	});
};

exports.queryDb = function(sql, data = [], returnAsObject = true) {
	let dbConn;

	return new Promise(function(resolve, reject) {
		exports
			.getConnection()
			.catch(function(err) {
				return reject("Error getting connection: " + err);
			})
			.then(function executeSQL(databaseConnection) {
				dbConn = databaseConnection;

				let options = {};
				if (returnAsObject) {
					options.outFormat = oracle.OUT_FORMAT_OBJECT;
				}

				return dbConn.execute(sql, data, options);
			})
			.then(function processResults(results) {
				resolve(results.rows);
			})
			.catch(function(error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function(err) {
				logger.warn("Unable to close database connection: " + err.message);
			});
	});
};

exports.executeSQL = function(sql, data = [], autoCommit = true) {
	let dbConn;

	return new Promise(function(resolve, reject) {
		exports
			.getConnection()
			.catch(function(err) {
				return reject("Error getting connection: " + err);
			})
			.then(function executeSQL(databaseConnection) {
				dbConn = databaseConnection;

				let options = {};
				if (autoCommit) {
					options.autoCommit = true;
				}

				return dbConn.execute(sql, data, options);
			})
			.then(function processResults(results) {
				resolve(results.rowsAffected);
			})
			.catch(function(error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function(err) {
				logger.warn("Unable to close database connection: " + err.message);
			});
	});
};
