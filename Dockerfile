FROM registry.gitlab.its.maine.edu/docker/node-base/12-oracle

WORKDIR /home/node/app
COPY libs ./libs/
RUN mkdir logs
COPY src ./src/
COPY .env ./
COPY package.json ./
COPY yarn.lock ./

RUN yarn --non-interactive --no-progress --prod --silent

COPY splunk.sh ./
RUN chmod +x ./splunk.sh
COPY docker/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
