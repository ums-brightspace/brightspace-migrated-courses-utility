#!/usr/bin/env bash

# Start app
yarn start &

# Start sending logs to splunk
sleep 10
./splunk.sh &

while sleep 60; do
  ps aux |grep yarn |grep -q -v grep
  if [ $? -ne 0 ]; then
    echo "Application has exited"
    exit 1
  fi

  ps aux |grep splunk |grep -q -v grep
  if [ $? -ne 0 ]; then
    echo "Splunk logging tail has exited"
    echo "Splunk logging tail has exited" >> logs/errorLog.log
  fi
done